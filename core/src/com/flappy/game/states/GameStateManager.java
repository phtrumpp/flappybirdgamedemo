package com.flappy.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

public class GameStateManager {

    private Stack<State> states;

    public GameStateManager() {
        states = new Stack<State>();
    }

    public void push(State state) {
        states.push(state);
    }

    public void pop() {
        states.pop();
    }

    /**
     * Setzt eine neue State (Z.B. Pause-state)
     * <p>
     * .pop entfernt die alte oberste und im nächsten Schritt .push fügt eine neue hinzu
     *
     * @param state
     */
    public void set(State state) {
        states.pop();
        states.push(state);

    }

    /**
     * peek()
     * Schaut sich das oberste Objekt des stacks an
     *
     * update() wendet die update Methode, welches ein delta übergibt, auf das Objekt an.
     *
     * @param dt
     */
    public void update(float dt) { // dt = delta

        states.peek().update(dt);

    }

    public void render(SpriteBatch sb) {
        states.peek().render(sb);
    }


}
