package com.flappy.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.flappy.game.FlappyBirdDemo;
import com.flappy.game.sprites.Bird;


public class PlayState extends State {

    private Bird bird;

    protected PlayState(GameStateManager gsm) {
        super(gsm);
        bird = new Bird(50,100);

        // --- Zoom into PLaystate --- //
        cam.setToOrtho(false, FlappyBirdDemo.WIDTH / 2 , FlappyBirdDemo.HEIGHT /2);
    }

    @Override
    protected void handleInput() {

        if (Gdx.input.justTouched()) {
            bird.jump();
        }
    }

    @Override
    protected void update(float dt) {
        handleInput();
        bird.update(dt);

    }

    @Override
    protected void render(SpriteBatch sb) {

        sb.setProjectionMatrix(cam.combined); // Zoom into PlayState !

        sb.begin();
        sb.draw(bird.getTexture(), bird.getPosition().x, bird.getPosition().y);
        sb.end();

    }

    @Override
    public void dispose() {

    }
}
