package com.flappy.game.states;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.flappy.game.FlappyBirdDemo;


public class MenuState extends State {

    private Texture backgroundTexture;
    private Texture playButton;

    public MenuState(GameStateManager gsm) { // needs to be a public constructor to use it in the create method
        super(gsm);
        backgroundTexture = new Texture("bg.png");
        playButton = new Texture("playbtn.png");

    }

    @Override
    public void handleInput() {

        if (Gdx.input.justTouched()){
            // if with mouseclick, fingerlick, whatever we change from Menustate to the following

            // --- changing Gamestate --- //
            gsm.set(new PlayState(gsm)); // creates new Playstate which gives back a new gamestatemanager
            dispose(); // get rif off individual texture, free memory
        }
    }

    @Override
    public void update(float dt) {
        handleInput(); // always checking our input if the users done anything

    }

    @Override
    public void render(SpriteBatch sb) {


        sb.begin();
        sb.draw(backgroundTexture,0,0, FlappyBirdDemo.WIDTH,FlappyBirdDemo.HEIGHT);
        sb.draw(playButton,(FlappyBirdDemo.WIDTH/2) - (playButton.getWidth() /2), FlappyBirdDemo.HEIGHT /2);
        sb.end();
    }

    @Override
    public void dispose() {
        backgroundTexture.dispose();
        playButton.dispose();
    }
}
