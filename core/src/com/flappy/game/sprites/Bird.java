package com.flappy.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.flappy.game.states.State;

/**
 *
 * Created by Philippe on 9/2/2020
 */
public class Bird {

    private static final int GRAVITY = -15;

    private Vector3 position;
    private Vector3 velocity;

    private Texture bird;

    public Bird (int x, int y) {
        position = new Vector3(x,y,0);
        velocity = new Vector3(0,0,0);
        bird = new Texture("bird.png");

    }

    public void update(float dt) {
        // everytime our bird is updated we want to add gravity to its velocity
        velocity.add(0,GRAVITY,0);
        velocity.scl(dt);
        // set up position
        position.add(0,velocity.y,0);

        velocity.scl(1/dt);
    }

    public Vector3 getPosition() {
        return position;
    }

    public Texture getTexture() {
        return bird;
    }
    public void jump() {
        velocity.y = 250;
    }
}
